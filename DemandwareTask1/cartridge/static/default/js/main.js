$(document).ready(function() {
			var submitButt = document.querySelector("input[type=submit]");		
			var form	   = document.querySelector('.dataForm')
			var response   = document.querySelector(".response");
			var customObjects = "retrieveCO-Start";
			var orderedList = document.querySelector("#myList");
			var selectElement = document.querySelector("select");
			var customObjectData = document.querySelector("ol");
			var people = $(customObjectData).children('li').get();
			var ajax = new XMLHttpRequest();
			
			
			
			//Create Custom Object Data
			$('form').validate();
			
			$('form').submit(function(e){				
				e.preventDefault();
				//current form email address
				var currentEmail = this[3];
				
				if($(this).valid()){
					var url = $(this).attr("action");
					var data = $(this).serialize();		
					
					$.ajax({
						type: "GET",
						url: customObjects,
						success: function(res){
							var people = $(res).find("#myList li").first();
							if($(people).data("email") == $(currentEmail).val()) {
								$(form).text("Email exists");
							} else {
								$.ajax({
									type: "POST",
									url: url,
									data: data + "&" + $(submitButt).attr('name') + "=" + $(submitButt).val(),
									success: function(result, status, callback){
										$(form).html($(result));
										response.innerHTML = '<p>See all people at <a href=' + "retrieveCO-Start" + '>Person Data Page</a></p>';							
									},
									error: function(err) {						
										console.log("an error occured");
										$(form).html($(err));
									}
								})
							}
						},
						error: function(res) {
							console.log(res);
						}
					});		
				}
			});
			// Show Custom Object Data
			$(selectElement).change(function() {
				
				var myLink = "retrieveCO-Start" + "?sortingID=" + $(this).val();
				
				$.ajax({
					type: "GET",
					url: myLink,
					success: function(res){
						$(customObjectData).html($(res).find("#myList > li"));
						hideElement();
					},
					error: function(res) {
						console.log(res);
					}
				});		
			});
			//hide checkboxes
			function hideElement(){
				$(".hideElement").click(function(){
					$(this.parentNode.parentNode.parentNode.nextSibling.nextSibling).toggle(450);
				});
			}
			
			function hideAll() {
				$(".hideAll").click(function() {
					$(orderedList).toggle(300);
				})
			}
			
			hideAll();
			hideElement();
});